(defproject com.thenicoteam.chatbot.gateway "0.1.0-SNAPSHOT"
  :description "Gateway to connect to XMPP and send HipChat API calls"
  :url "http://example.com/FIXME"
  :license {:name "Proprietary"}
  :repositories {"Boxtrot Studio" "https://repo.boxtrotstudio.com/maven"}
  :main com.thenicoteam.chatbot.gateway.core/main
  :dependencies [[clj-http "3.6.1"]
                 [com.novemberain/langohr "4.1.0"]
                 [org.clojure/clojure "1.8.0"]
                 [org.clojure/data.json "0.2.6"]
                 [com.ep.hippyjava/HippyJava "1.0.0-SNAPSHOT"]])
