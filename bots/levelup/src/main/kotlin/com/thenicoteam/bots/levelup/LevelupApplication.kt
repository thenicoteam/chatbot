package com.thenicoteam.bots.levelup

import org.springframework.boot.SpringApplication
import org.springframework.boot.autoconfigure.SpringBootApplication

@SpringBootApplication
class LevelupApplication

fun main(args: Array<String>) {
    SpringApplication.run(LevelupApplication::class.java, *args)
}
