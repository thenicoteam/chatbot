package com.thenicoteam.chatbot.router.router

import com.thenicoteam.chatbot.router.common.ChatBot
import com.thenicoteam.chatbot.router.common.ChatMessage
import com.thenicoteam.chatbot.router.common.ChatMessageBody
import org.apache.camel.EndpointInject
import org.apache.camel.Handler
import org.apache.camel.ProducerTemplate
import org.springframework.beans.factory.annotation.Value
import org.springframework.context.ApplicationContext
import org.springframework.stereotype.Component

@Component
class Router(
        @Value("\${chatbot.name}") val botName: String,
        val gateway: Gateway,
        val applicationContext: ApplicationContext
) {

    var definitions = mapOf(
            "ping" to ServiceDefinition("ping", Transport.BEAN, "pingBot"),
            "levelup" to ServiceDefinition("levelup", Transport.QUEUE, "levelup")
    )

    @Handler
    fun route(chatMessage: ChatMessage) {
        val message = chatMessage.payload

        if (message.transportId == botName) return

        if (!message.message.startsWith("@" + botName)) {
            gateway.sendToFireHose(message)

            return
        }

        val parts = message.message.split(" ", ignoreCase =  true, limit = 3)

        val serviceName = parts.getOrElse(1, {""})
        val messageText = parts.getOrElse(2, {""})

        if (serviceName.isEmpty()) {
            replyErrorMessage(message, "Service name must be provided")

            return
        }

        if (definitions.containsKey(serviceName)) {

            val service = definitions[serviceName]


            when (service?.transport) {
                Transport.BEAN -> routeToBean(service.route, message, messageText)
                Transport.QUEUE -> {
                    val newMessage = ChatMessageBody(
                            message.transportType, message.transportId,
                            message.messageSource, message.messageSourceId, messageText
                    )
                    routeToQueue(service.route, newMessage)
                }
                Transport.HTTP -> TODO()
                else -> replyErrorMessage(message, "Service $serviceName is not defined")
            }
        } else {
            replyErrorMessage(message, "(wat)")
        }
    }

    private fun routeToBean(beanName: String, messageBody: ChatMessageBody, message: String) {
        val bean: ChatBot = applicationContext.getBean(beanName) as ChatBot

        bean.handleMessage(
                ChatMessageBody(messageBody.transportType, messageBody.transportId,
                        messageBody.messageSource, messageBody.messageSourceId, message)
        )
    }

    private fun routeToQueue(queueRoutingKey: String, messageBody: ChatMessageBody) {
        gateway.sendToRoute(messageBody, queueRoutingKey)
    }

    private fun replyErrorMessage(message: ChatMessageBody, text: String) {
        gateway.reply(
                ChatMessageBody(
                        message.transportType, message.transportId,
                        message.messageSource, message.messageSourceId, text
                )
        )
    }

}
