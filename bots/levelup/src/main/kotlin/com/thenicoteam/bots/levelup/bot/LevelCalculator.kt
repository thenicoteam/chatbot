package com.thenicoteam.bots.levelup.bot

interface LevelCalculator {

    fun calculate(numberOfMessages: Int) : Int
}
