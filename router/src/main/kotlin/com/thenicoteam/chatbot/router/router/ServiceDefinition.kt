package com.thenicoteam.chatbot.router.router

enum class Transport { QUEUE, HTTP, BEAN }

data class ServiceDefinition(
        val serviceIdentifier: String,
        val transport: Transport,
        val route: String
)
