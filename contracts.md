# Contracts

## Gateway

### To Router
```
{
  "headers": {
    "content-type": "message/chat"
  },
  "payload": {
    "transport-type": "HIPCHAT",
    "transport-id": "UserName",
    "message-source": "ROOM",
    "message-source-id": "UserName or RoomName",
    "message": "@ChatBot ping"
  }
}
```
#### where

`sender-type`: application message was sent from

* `HIPCHAT`

`sender-id`: identifier of the user in the target application

`message-source`: where message was posted to

* `ROOM`
* `DIRECT`

`message-source-id`:

* identifier of Room if message-source is ROOM
* identifier of User if message-source is DIRECT

`message`: content of posted message
