package com.thenicoteam.bots.levelup

import com.fasterxml.jackson.annotation.JsonCreator
import com.fasterxml.jackson.annotation.JsonProperty

enum class TransportType { HIPCHAT }
enum class MessageSource { ROOM, DIRECT }

data class Message @JsonCreator constructor(
        @JsonProperty("headers") val headers: Map<String, String>,
        @JsonProperty("payload") val payload: MessagePayload
)

data class MessagePayload @JsonCreator constructor(
        @JsonProperty("transport-type") @field:JsonProperty("transport-type") val transportType: TransportType,
        @JsonProperty("transport-id") @field:JsonProperty("transport-id") val transportId: String,
        @JsonProperty("message-source") @field:JsonProperty("message-source") val messageSource: MessageSource,
        @JsonProperty("message-source-id") @field:JsonProperty("message-source-id") val messageSourceId: String,
        @JsonProperty("message") val message: String
)
