package com.thenicoteam.bots.levelup.bot

import org.springframework.stereotype.Component

@Component
class LogLevelCalculator : LevelCalculator {
    override fun calculate(numberOfMessages: Int): Int {
        return Math.ceil(Math.log((numberOfMessages / 5).toDouble())).toInt() + 1
    }
}
