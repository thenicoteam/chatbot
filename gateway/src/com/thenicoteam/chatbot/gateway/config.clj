(ns com.thenicoteam.chatbot.gateway.config
  (:gen-class)
  (:require [clojure.data.json :as json]))

(def ^{:const true} config-file-path "config/main.json")

(defn parse-config
  "Gets the JSON config as a map."
  []
  (try (json/read-str (slurp config-file-path))
       (catch java.io.FileNotFoundException e nil)))

(defn hipchat-config
  "Gets the HipChat config."
  []
  (get (parse-config) "hipchat"))

(defn rabbit-config
  "Gets the RabbitMQ config."
  []
  (get (parse-config) "rabbit"))
