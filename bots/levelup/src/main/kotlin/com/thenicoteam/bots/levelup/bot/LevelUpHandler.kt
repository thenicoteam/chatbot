package com.thenicoteam.bots.levelup.bot

import com.thenicoteam.bots.levelup.Message
import com.thenicoteam.bots.levelup.MessagePayload
import com.thenicoteam.bots.levelup.entity.UserEntry
import com.thenicoteam.bots.levelup.entity.UserEntryRepository
import org.apache.camel.EndpointInject
import org.apache.camel.ProducerTemplate
import org.springframework.stereotype.Component

@Component
class LevelUpHandler(
        val repository: UserEntryRepository,
        val levelCalculator: LevelCalculator
) {

    @EndpointInject(uri = "direct:replyGateway")
    private lateinit var replyGateway: ProducerTemplate

    fun handleHoseMessage(message: Message) {
        var entry = repository.findByUserId(message.payload.transportId)

        if (entry == null) {
            entry = UserEntry(message.payload.transportId, 0)
        }

        val beforeLevel = levelCalculator.calculate(entry.counter)

        entry.increment()

        repository.save(entry)

        val afterLevel = levelCalculator.calculate(entry.counter)

        if (afterLevel > beforeLevel) {
            replyGateway.sendBody(Message(
                    mapOf("content-type" to "message/notification"),
                    MessagePayload(
                            message.payload.transportType, message.payload.transportId, message.payload.messageSource,
                            message.payload.messageSourceId, "Congratulations! You've reached level $afterLevel"
                    )
            ))
        }

    }
}
