package com.thenicoteam.chatbot.router.integration

import com.thenicoteam.chatbot.router.common.ChatMessage
import com.thenicoteam.chatbot.router.common.ChatMessageBody
import com.thenicoteam.chatbot.router.router.Gateway
import org.apache.camel.EndpointInject
import org.apache.camel.ProducerTemplate
import org.springframework.stereotype.Component

@Component
class CamelRabbitmqGateway: Gateway {

    @EndpointInject(uri = "direct:fireHose")
    lateinit var fireHose: ProducerTemplate

    @EndpointInject(uri = "direct:replyGateway")
    lateinit var replyGateway: ProducerTemplate

    @EndpointInject(uri = "direct:bots")
    lateinit var botsGateway: ProducerTemplate

    override fun sendToFireHose(chatMessageBody: ChatMessageBody) {
        val message = ChatMessage(mapOf("ContentType" to "message/chat-message"), chatMessageBody)

        fireHose.sendBody(message)
    }

    override fun sendToRoute(message: ChatMessageBody, routingKey: String) {
        val chatMessage = ChatMessage(mapOf("ContentType" to "message/chat-message"), message)

        botsGateway.sendBodyAndHeaders(chatMessage, mutableMapOf(
                "rabbitmq.ROUTING_KEY" to routingKey
        ) as Map<String, Any>?)
    }

    override fun reply(chatMessageBody: ChatMessageBody) {
        val message = ChatMessage(mapOf("ContentType" to "message/chat-message-reply"), chatMessageBody)

        replyGateway.sendBody(message)
    }
}
