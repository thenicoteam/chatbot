# Router

Router taking care about routing messages to proper registered bots.

Written with Kotlin <3, Spring Boot and Camel

### Included bots

#### Ping
`@Bot ping` will reply with `Pong`

#### Service
Bot taking care of service registry. WIP

### Running

You may or may not need Gradle installed.

```
gradle bootRun
```
