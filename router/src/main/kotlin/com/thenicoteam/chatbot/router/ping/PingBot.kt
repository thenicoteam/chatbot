package com.thenicoteam.chatbot.router.ping

import com.thenicoteam.chatbot.router.common.ChatBot
import com.thenicoteam.chatbot.router.common.ChatMessageBody
import com.thenicoteam.chatbot.router.router.Gateway
import org.springframework.stereotype.Component

@Component
class PingBot(val gateway: Gateway) : ChatBot {

    override fun handleMessage(message: ChatMessageBody) {
        gateway.reply(ChatMessageBody(
                message.transportType, message.transportId,
                message.messageSource, message.messageSourceId, "Pong!"
        ))
    }
}
