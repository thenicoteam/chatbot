(ns com.thenicoteam.chatbot.gateway.core
  (:gen-class)
  (:require [com.thenicoteam.chatbot.gateway.config :as config]
            [com.thenicoteam.chatbot.gateway.bot    :as bot]
            [clojure.data.json                      :as json]
            [langohr.core                           :as lango-core]
            [langohr.consumers                      :as lango-consumers]
            [langohr.channel                        :as lango-channel]
            [langohr.exchange                       :as lango-exchange]
            [langohr.basic                          :as lango-basic]
            [langohr.queue                          :as lango-queue])
  (:import com.ep.hippyjava.bot.HippyBot)
  (:import com.ep.hippyjava.HippyJava))

(def ^{:const true} inbound-exchange-name "com.thenicoteam.chatbot.exchange.inbound-messages")
(def ^{:const true} outbound-exchange-name "com.thenicoteam.chatbot.exchange.outbound-messages")
(def ^{:const true} inbound-queue-name "com.thenicoteam.chatbot.queue.inbound-messages")
(def ^{:const true} outbound-queue-name "com.thenicoteam.chatbot.queue.outbound-messages")

(defn jsonify-message
  [message from room]
  (json/write-str {:headers {:content-type "message/chat"}
                   :payload {:transport-type "HIPCHAT"
                             :transport-id (. from getMentionName)
                             :message-source "ROOM"
                             :message-source-id (. room getTrueName)
                             :message message}}))

(defn handle-hipchat-message
  "Handle an XMPP message."
  [channel message from room]
  (lango-basic/publish channel inbound-exchange-name "" (jsonify-message message from room)))

(defn handle-outbound-message
  "Handle a message from RabbitMQ."
  [bot channel meta ^bytes raw]
  (let [payload   (get (json/read-str (String. raw "UTF-8")) "payload")
        room-name (get payload "message-source-id")
        user-name (get payload "transport-id")
        message   (get payload "message")]
    (.sendMessageToRoom bot room-name (str "@" user-name " " message))))

(defn main
  "I don't do a whole lot."
  []
  (let [connection (lango-core/connect {:username (get (config/rabbit-config) "username")
                                        :password (get (config/rabbit-config) "password")})
        channel    (lango-channel/open connection)
        in-queue   (.getQueue (lango-queue/declare channel inbound-queue-name {:durable true
                                                                               :auto-delete false}))
        out-queue  (.getQueue (lango-queue/declare channel outbound-queue-name {:durable true
                                                                                :auto-delete false}))
        bot        (bot/create-hipchat-bot channel handle-hipchat-message)]
    (lango-exchange/declare channel inbound-exchange-name "direct" {:durable true})
    (lango-queue/bind channel in-queue inbound-exchange-name)
    (lango-exchange/declare channel outbound-exchange-name "direct" {:durable true})
    (lango-queue/bind channel out-queue outbound-exchange-name)
    (lango-consumers/subscribe channel
                               outbound-queue-name
                               (partial handle-outbound-message bot)
                               {:auto-ack true})
    (HippyJava/runBot bot)))
