(ns com.thenicoteam.chatbot.gateway.bot
  (:gen-class)
  (:require [com.thenicoteam.chatbot.gateway.config :as config]
            [clojure.data.json                      :as json])
  (:import com.ep.hippyjava.bot.HippyBot))

(defn create-hipchat-bot
  "Gets an anonymous instance of HippyBot."
  [channel receive-message-callback]
  (proxy [HippyBot] []
    (username []
      (get (config/hipchat-config) "username"))
    (password []
      (get (config/hipchat-config) "password"))
    (nickname []
      (get (config/hipchat-config) "nickname"))
    (apiKey []
      (get (config/hipchat-config) "api-key"))
    (receiveMessage [message from room]
      (receive-message-callback channel
                                message
                                (proxy-super findUser from)
                                room))
    (onLoad []
      (do
        (proxy-super joinRoom "Thenicoteam")
        (proxy-super changeRoom "Thenicoteam")))))
