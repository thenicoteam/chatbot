package com.thenicoteam.chatbot.router

import org.springframework.boot.SpringApplication
import org.springframework.boot.autoconfigure.SpringBootApplication

@SpringBootApplication
class RouterApplication

fun main(args: Array<String>) {
    SpringApplication.run(RouterApplication::class.java, *args)
}
