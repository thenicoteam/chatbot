package com.thenicoteam.chatbot.router.integration

import com.thenicoteam.chatbot.router.common.ChatMessage
import com.thenicoteam.chatbot.router.router.Router
import org.apache.camel.CamelContext
import org.apache.camel.builder.RouteBuilder
import org.apache.camel.model.dataformat.JsonLibrary
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Component

@Component
class FireHoseRoute(context: CamelContext?) : RouteBuilder(context) {
    override fun configure() {
        from("direct:fireHose")
                .marshal().json(JsonLibrary.Jackson)
                .to(rabbitmqPublisherUri("com.thenicoteam.chatbot.exchange.thehose", "", "fanout"))

    }
}

@Component
class BotsRoutedRoute(context: CamelContext?) : RouteBuilder(context) {
    override fun configure() {
        from("direct:bots")
                .marshal().json(JsonLibrary.Jackson)
                .to(rabbitmqPublisherUri("com.thenicoteam.chatbot.exchange.bots", ""))
    }
}

@Component
class ReplyGatewayRoute(context: CamelContext?) : RouteBuilder(context) {
    override fun configure() {
        from("direct:replyGateway")
                .marshal().json(JsonLibrary.Jackson)
                .to(rabbitmqPublisherUri(
                        "com.thenicoteam.chatbot.exchange.outbound-messages",
                        ""
                ))
    }
}

@Component
class GatewayMessageConsumerRoute(context: CamelContext?) : RouteBuilder(context) {

    @Autowired
    private lateinit var router: Router

    override fun configure() {
        from(rabbitmqConsumerUri(
                "com.thenicoteam.chatbot.exchange.inbound-messages",
                "com.thenicoteam.chatbot.queue.inbound-messages"
            ))
                .unmarshal().json(JsonLibrary.Jackson, ChatMessage::class.java)
                .bean(router, "route")
    }
}

fun rabbitmqPublisherUri(exchange: String, routingKey: String, exchangeType: String = "direct") : String {
    return "rabbitmq://localhost/$exchange?routingKey=$routingKey&exchangeType=$exchangeType&${rabbitmqCommon()}"
}

fun rabbitmqConsumerUri(exchange: String, queueName: String, exchangeType: String = "direct"): String {
    return "rabbitmq://localhost/$exchange?queue=$queueName&exchangeType=$exchangeType&${rabbitmqCommon()}"
}

fun rabbitmqCommon(): String {
    return "username=rabbitmq&password=rabbitmq&autoDelete=false"
}
