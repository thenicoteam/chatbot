package com.thenicoteam.bots.levelup.entity

import org.springframework.data.repository.CrudRepository
import org.springframework.stereotype.Repository
import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.Id
import javax.persistence.Table

@Entity
@Table(name = "user_room_entry")
class UserEntry(
        @Id @Column(name = "user_id") val userId: String,
        var counter: Int = 1
) {
    fun increment() {
        this.counter++
    }
}


@Repository
interface UserEntryRepository : CrudRepository<UserEntry, String> {
    fun findByUserId(userId: String): UserEntry?
}
