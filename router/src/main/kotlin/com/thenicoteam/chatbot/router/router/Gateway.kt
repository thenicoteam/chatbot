package com.thenicoteam.chatbot.router.router

import com.thenicoteam.chatbot.router.common.ChatMessageBody

interface Gateway {

    fun sendToFireHose(chatMessageBody: ChatMessageBody)

    fun sendToRoute(message: ChatMessageBody, routingKey: String)

    fun reply(chatMessageBody: ChatMessageBody)
}
