package com.thenicoteam.bots.levelup.integration

import com.thenicoteam.bots.levelup.Message
import com.thenicoteam.bots.levelup.bot.LevelUpHandler
import org.apache.camel.CamelContext
import org.apache.camel.builder.RouteBuilder
import org.apache.camel.model.dataformat.JsonLibrary
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Component

@Component
class InboundHoseRoute(context: CamelContext?) : RouteBuilder(context) {

    @Autowired
    private lateinit var handler: LevelUpHandler

    override fun configure() {
        from(rabbitmqConsumerUri(
                "com.thenicoteam.chatbot.exchange.thehose",
                "com.thenicoteam.chatbot.queue.levelup",
                "fanout"
        ))
                .unmarshal().json(JsonLibrary.Jackson, Message::class.java)
                .bean(handler, "handleHoseMessage")
    }
}

@Component
class ReplyGatewayRoute(context: CamelContext?) : RouteBuilder(context) {
    override fun configure() {
        from("direct:replyGateway")
                .marshal().json(JsonLibrary.Jackson)
                .to(rabbitmqPublisherUri(
                        "com.thenicoteam.chatbot.exchange.outbound-messages",
                        ""
                ))
    }
}

fun rabbitmqConsumerUri(exchange: String, queueName: String, exchangeType: String): String {
    return "rabbitmq://localhost/$exchange?queue=$queueName&exchangeType=$exchangeType&${rabbitmqCommon()}"
}

fun rabbitmqPublisherUri(exchange: String, routingKey: String, exchangeType: String = "direct") : String {
    return "rabbitmq://localhost/$exchange?routingKey=$routingKey&exchangeType=$exchangeType&${rabbitmqCommon()}"
}

fun rabbitmqCommon(): String {
    return "username=rabbitmq&password=rabbitmq&autoDelete=false"
}
