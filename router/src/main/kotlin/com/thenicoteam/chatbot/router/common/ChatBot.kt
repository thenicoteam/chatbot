package com.thenicoteam.chatbot.router.common

interface ChatBot {
    fun handleMessage(message: ChatMessageBody)
}
